package com.example.luizalabs.teste.respository;

import com.example.luizalabs.teste.model.Comunicacao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface DBRepository extends CrudRepository<Comunicacao, UUID> {

    Optional<Comunicacao> findByUuid ( UUID uuid );

    void deleteByUuid(UUID uuid );
}
