package com.example.luizalabs.teste.controller;


import com.example.luizalabs.teste.model.Comunicacao;
import com.example.luizalabs.teste.services.ComunicacaoService;
import io.swagger.annotations.Api;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Log4j2
@Api
@RestController
@RequestMapping("/api/v1/comunicacao/")
public class ComunicationController {

    private static final Logger logger = LoggerFactory.getLogger(ComunicationController.class);

    @Autowired
    private ComunicacaoService comunicacaoService;

    @PostMapping(value = "/agenda", consumes = "application/json")
    @ResponseBody
    public ResponseEntity agendaComunicacao(@RequestBody Comunicacao comunicacao){

       try{
           if(comunicacaoService.isJSONValid(comunicacao.toString())){
               comunicacaoService.criaCoumunicacao(comunicacao);
               URI location = new URI("/agenda");
               return ResponseEntity.created(location).build();
           }
            return ResponseEntity.badRequest().body(null);
       }catch(Exception e) {
            logger.error("JSON fields are not parsable. " + e);
            return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(null);
        }
    }

    @GetMapping(value = "/consulta/{uuid}")
    public ResponseEntity<Comunicacao> consultaComunicacao(@PathVariable UUID uuid){
      return comunicacaoService.obtemUmaComunicacao(uuid)
              .map(record -> ResponseEntity.ok().body(record))
              .orElse(ResponseEntity.notFound().build());
    }


    @DeleteMapping(value = "/{uuid}")
    public ResponseEntity<UUID> deletePost(@PathVariable UUID uuid) {
        try{
            comunicacaoService.delete(uuid);
            return ResponseEntity.noContent().build();
        }catch(Exception e) {
            logger.error(e.toString());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
        }
    }
}
