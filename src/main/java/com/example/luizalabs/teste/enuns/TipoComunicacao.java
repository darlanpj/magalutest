package com.example.luizalabs.teste.enuns;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public enum TipoComunicacao {

    EMAIL("EMAIL"), SMS("SMS"), PUSH("PUSH"), WHATSAPP("WHATSAPP");

    private String tipo;;

    TipoComunicacao(String tipoCom) {
        tipo = tipoCom;
    }
}
