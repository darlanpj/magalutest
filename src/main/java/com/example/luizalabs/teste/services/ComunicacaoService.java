package com.example.luizalabs.teste.services;

import com.example.luizalabs.teste.enuns.TipoComunicacao;
import com.example.luizalabs.teste.model.Comunicacao;
import com.example.luizalabs.teste.respository.DBRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.io.IOException;
import java.util.Optional;
import java.util.UUID;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class ComunicacaoService{

    private static final Logger logger = Logger.getLogger(ComunicacaoService.class.getName());
    private static final String REGEX_MATCHER_THIRTEEN_DIGITS_NUMBER = "\\d{2}-\\d{2}-\\d{9}";

    @Autowired
    private DBRepository respository;

    @Transactional
    public void criaCoumunicacao(Comunicacao comunicacao) {

        if(comunicacao.getTipocomunicacao().getTipo().equals("EMAIL") &&
           isEmail(comunicacao.getDestinatario()) ){
            respository.save(comunicacao);
            logger.info("Destinatário tipo - E-mail!");
            logger.info("Comunicação agendada!");
        } else if (isPhone(comunicacao.getDestinatario())) {
            respository.save(comunicacao);
            logger.info("Destinatário tipo - Telefone!");
            logger.info("Comunicação agendada!");
        }
    }

    public Optional<Comunicacao> obtemUmaComunicacao(UUID uuid) {
        return respository.findByUuid(uuid);
    }

    @Transactional
    public boolean delete(UUID uuid) {

        var umaComunicacao = respository.findByUuid(uuid);
        if (umaComunicacao.isPresent()){
            respository.deleteByUuid(uuid);
            logger.info("Comunicação com {uuid} apagada");
            return true;
        }
        else{
            logger.info("Comunicação não encontrada");
            return false;
        }
    }

    public boolean isJSONValid(String jsonInString) {
        try {
            return new ObjectMapper().readTree(jsonInString) != null;
        } catch (IOException e) {
            return false;
        }
    }

    public static boolean isPhone(String tipoComunicacaoPhone) {
        Pattern pattern = Pattern.compile(REGEX_MATCHER_THIRTEEN_DIGITS_NUMBER);
        Matcher matcher = pattern.matcher(tipoComunicacaoPhone);
        return matcher.matches();
    }

    public static boolean isEmail(String tipoComunicacaoEmail) {

        return EmailValidator.getInstance().isValid(tipoComunicacaoEmail);
    }
}
