package com.example.luizalabs.teste;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories(basePackages = "com.example.luizalabs.teste")
public class TesteApplication {

	public static void main(String[] args) {

		SpringApplication.run(TesteApplication.class, args);
	}

}
