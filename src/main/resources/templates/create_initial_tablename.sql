CREATE TABLE IF NOT EXISTS comunicacao (
	id int(10) NOT NULL AUTO_INCREMENT,
    dataHora datetime,
	destinatario varchar(100),
    mensagem varchar(500),
	tipoComunicacao varchar(50),
	PRIMARY KEY (id)
) ;
